# Corruption & military expenditure #
## What is in the directories? 

In the resources map you will find the following:

### original_files: 
* the original excel and csv files as downloaded from their respective websites
* download links for these files.
### preprocessing: 
* both datasets as csv files
* the shellscript fix.sh that does the majority of the preprocessing 
* two python files used for this script
### final_tables:
* the two csv files after preprocessing (same csv files as in analyze directory, here for backup)
### analyze:  
* final csv files after preprocessing
* python program 'average.py' used to calculate the per year averages for each csv file
* python program 'occur.py' used to get the final number of occurences per group, for the 2x2 table
		
### How do I exactly replicate the results of your research?
* download the military expenditure expenditure csv table and the CPI excel table from the links in the original files directory
* convert the historical tab of the CPI2017 excel file to csv format using this site https://convertio.co/nl/xlsx-converter/
* run the script fix.sh located in the preprocessing directory on the files (check the program itself for the exact usage). This will do most of the preprocessing.
* manually remove the entries for Korea, Brunei, Swaziland and the Seychelles in the newly created files (they are incomplete/cause problems) 
* rearrange the CPI columns so they match the military expenditure table with this tool (swap columns 2-5 and 3-4): https://www.browserling.com/tools/csv-swap-columns
* For some reason this tool adds back the quotation removed by the shellscript. Use $tr -d '"' on the CPI table to get rid of them.
* the following programs are located in the 'analyze' directory
* the python program average.py produces averages from the previous csv files. Running this program is not needed since these averages are already incorporated in occur.py
* use the program occur.py to print the amount of occurences for each group. 
* It prints these occurences in the following format:(low-corruption low-spending 2013 2014) (low-corruption low-spending 2015-2016) (high-corruption high spending 2013-2014) (high-corruption high-spending 2015-2016)
* use these numbers as input for the 2x2 table on https://www.graphpad.com/quickcalcs/contingency1/ as shown in the analysis section

