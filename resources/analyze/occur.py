#!/usr/bin/python3
#calculates groups of countries with low-corruption and low military spending
#and high-corruption and high military spending
#divided into time periods 2013-2014 and 2015-2016
#based on the files final_cpi.csv and final_mil.csv
#note that these files have to be present in the same directory as this program

import sys

def main():
    groupA = []
    groupB = []
    groupC = []
    groupD = []
    with open("final_cpi.csv", "r") as cpi:
    #categorization based on cpi
        for line in cpi:
            line = line.strip().split(',')
            if float(line[1]) > 44.30578512396694 or float(line[2]) > 44.82644628099174:
                groupA.append(line[0])
            else: 
                groupC.append(line[0])

            if float(line[3]) > 44.95867768595041 or float(line[4]) > 44.768595041322314:
                groupB.append(line[0])
            else: 
                groupD.append(line[0])
 
    with open("final_mil.csv", "r") as mil:
        #further filtering based on military expenditure
            for line in mil:
                line = line.strip().split(',')
                #removing high spending from low spending groups
                if float(line[1]) > 1.8343240949197572 or float(line[2]) > 1.8309516130185204:
                    if line[0] in groupA:
                        groupA.remove(line[0])                        
                if float(line[3]) > 1.91771704454159 or float(line[4]) > 1.852395892205541:
                    if line[0] in groupB:
                        groupB.remove(line[0])
            #removing low spending from high spending groups
                if float(line[1]) < 1.8343240949197572 or float(line[2]) < 1.8309516130185204:
                    if line[0] in groupC:
                        groupC.remove(line[0])
                if float(line[3]) < 1.91771704454159 or float(line[4]) < 1.852395892205541: 
                    if line[0] in groupD:
                        groupD.remove(line[0])
    print(len(groupA), len(groupB), len(groupC), len(groupD))
main()
    

            
