#!/usr/bin/python3

import sys

def main():
    with open(sys.argv[1]) as file1:
        sum2013 = 0
        sum2014 = 0
        sum2015 = 0
        sum2016 = 0
        count = 0
        for line in file1:
            count += 1
            line = line.strip().split(',')
            sum2013 += float(line[1])
            sum2014 += float(line[2])
            sum2015 += float(line[3])
            sum2016 += float(line[4])

    print("2013: {0}, 2014: {1}, 2015: {2}, 2016: {3}".format(sum2013 / count, sum2014 / count, sum2015 / count, sum2016 / count))
main()
