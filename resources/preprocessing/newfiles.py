#!/usr/bin/python3
import sys

def main():
    #generate new csv files based on the list of common countries
    with open("temp_file3","r") as tempfile3:
        countries = []
        for line in tempfile3:
            line = line.strip()
            countries.append(line)
        with open("temp_file1", "r") as tempfile1:
            for line in tempfile1:
                linelist = line.strip().split(',')
                if linelist[0] in countries:
                    with open("temp_file4","a") as tempfile4:
                        tempfile4.write(line)
        with open(sys.argv[1], "r") as file2:
            for line in file2:
                linelist2 = line.strip().split(',')
                if linelist2[0] in countries:
                    with open("temp_file5","a") as tempfile5:
                        tempfile5.write(line)
main()
