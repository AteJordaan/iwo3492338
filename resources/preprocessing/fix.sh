#!/bin/bash
#
#script to preprocess a CPI csv table and a military expenditure csv table
#
#usage: ./fix.sh FILE1 FILE2
#
#note that this script is specifically written for mil_exp.csv as FILE1
#and cpi.csv as FILE2
#both filter.py and newfiles.py need to be present in the same directory as this script 
#in order to function

temp_file1=$(mktemp)
temp_file2=$(mktemp)
temp_file3=$(mktemp)
temp_file4=$(mktemp)
temp_file5=$(mktemp)
python filter.py $1
cut -d ',' -f1 temp_file1 > temp_file2
cut -d ',' -f1 $2 >> temp_file2
cat temp_file2 | sort | uniq -d > temp_file3
python newfiles.py $2
cut -d ',' -f1,58,59,60,61 temp_file4 | sort | tr -d '"' > final_mil.csv
cut -d ',' -f1,7,10,13,16 temp_file5 | sort | tr -d '"' > final_cpi.csv 
rm temp_file1
rm temp_file2
rm temp_file3
rm temp_file4
rm temp_file5


