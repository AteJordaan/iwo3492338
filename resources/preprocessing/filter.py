#!/usr/bin/python3
#to be used in the fix.sh script
import sys

def main():
    #remove countries with no data on 2013-2016
    with open(sys.argv[1], "r") as file1:
        for line in file1:
            linelist = line.strip().split(',')            
            if linelist[60] != '""'  and \
            linelist[59] != '""' and \
            linelist[58] != '""' and \
            linelist[57] != '""':
                with open("temp_file1", "a") as tempfile1:
                    tempfile1.write(line)
main()
                    
                
